// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class GD53_Final_WubboEditorTarget : TargetRules
{
	public GD53_Final_WubboEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		ExtraModuleNames.Add("GD53_Final_Wubbo");
	}
}
