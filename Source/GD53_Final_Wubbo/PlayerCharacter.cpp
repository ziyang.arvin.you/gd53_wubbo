// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerCharacter.h"
#include "Engine/World.h"

//APlayerCharacter::APlayerCharacter()
//{
//	Super();
//	_Gravity = GetWorld()->GetGravityZ();
//	_CharacterHalfHeight = 80;
//}

void APlayerCharacter::UsingGrappleHook(AActor* TargetPoint)
{
	/*
	if (TargetPoint != NULL) {
		FString Text = TargetPoint->GetName();
		UE_LOG(LogTemp, Warning, TEXT("TargetPoint's name is: %s"), *Text)
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("TargetPoint is null"))
	}
	*/

	FVector TargetLocation = TargetPoint->GetActorLocation();
	FVector TargetDirction = TargetLocation - this->GetActorLocation();
	
	float TimeInterval;
	float VelocityZ = (float)sqrt(_Gravity * 2 * (_CharacterHalfHeight + TargetDirction.Z));
	TimeInterval = VelocityZ / _Gravity;
	float VelocityX = TargetDirction.X / TimeInterval;
	float VelocityY = TargetDirction.Y / TimeInterval;

	SetUpVelocity(VelocityX, VelocityY, VelocityZ);
}

void APlayerCharacter::SetUpVelocity(float VelocityX, float VelocityY, float VelocityZ)
{
	//Velocity = *new FVector(VelocityX, VelocityY, VelocityZ);
}