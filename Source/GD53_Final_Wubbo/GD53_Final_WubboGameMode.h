// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GD53_Final_WubboGameMode.generated.h"

UCLASS(minimalapi)
class AGD53_Final_WubboGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGD53_Final_WubboGameMode();
};



