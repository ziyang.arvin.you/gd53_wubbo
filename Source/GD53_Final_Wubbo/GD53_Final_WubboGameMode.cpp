// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "GD53_Final_WubboGameMode.h"
#include "GD53_Final_WubboCharacter.h"
#include "PlayerCharacter.h"
#include "UObject/ConstructorHelpers.h"

AGD53_Final_WubboGameMode::AGD53_Final_WubboGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Character/Player/BP_MyPlayerCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
