// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GD53_Final_WubboCharacter.h"
#include "PlayerCharacter.generated.h"

/**
 * 
 */
UCLASS()
class GD53_FINAL_WUBBO_API APlayerCharacter : public AGD53_Final_WubboCharacter
{
	GENERATED_BODY()
	
public:

	//APlayerCharacter();

protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "PlayerJumpStatus")
	FVector Velocity;

	UFUNCTION(BlueprintCallable, Category = "GrappleHook")
	void UsingGrappleHook(AActor* TargetPoint);

private:

	void SetUpVelocity(float, float, float);

	float _Gravity;

	float _CharacterHalfHeight;
};