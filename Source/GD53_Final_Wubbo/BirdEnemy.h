// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnemyAbstract.h"
#include "BirdEnemy.generated.h"

/**
 * 
 */
UCLASS()
class GD53_FINAL_WUBBO_API ABirdEnemy : public AEnemyAbstract
{
	GENERATED_BODY()
	
};
