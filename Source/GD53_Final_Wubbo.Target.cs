// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class GD53_Final_WubboTarget : TargetRules
{
	public GD53_Final_WubboTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("GD53_Final_Wubbo");
	}
}
